import { Column, Entity, ObjectIdColumn } from 'typeorm';
import { ObjectID } from 'typeorm';
import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@Entity('User')
export class User {
  @ObjectIdColumn()
  id: ObjectID;

  @IsString()
  @Column()
  userId: string;

  @IsString()
  @IsNotEmpty()
  @Column()
  @ApiProperty({
    description: 'Họ và tên',
    default: 'Nguyễn Đức',
  })
  name: string;

  @IsNotEmpty()
  @IsEmail()
  @Column()
  @ApiProperty({
    description: 'Địa chỉ email',
    default: 'demo@gmail.com',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @Column()
  @ApiProperty({
    description: 'Mật khẩu định đạng MD5',
    default: 'e10adc3949ba59abbe56e057f20f883e',
  })
  password: string;

  @IsString()
  @Column()
  @ApiProperty({
    description: 'Đường dẫn ảnh avatar',
    default: 'https://i.imgur.com/XwubuzF.jpg',
  })
  avatar: string;

  // @IsPhoneNumber('VI', { message: 'Số điện thoại cần đúng định dạng' })
  @Column()
  @ApiProperty({
    description: 'Số điện thoại',
    default: '0898572528',
  })
  phone: string;

  @IsNumber()
  @Column()
  createdAt: number;

  token: string;

  constructor(user?: Partial<User>) {
    Object.assign(this, user);
  }

  toJSON() {
    delete this.password;
    return this;
  }
}
