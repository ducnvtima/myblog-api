import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { MongoRepository } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: MongoRepository<User>,
  ) {}

  create(createUserDto: CreateUserDto) {
    return this.userRepository.save(createUserDto);
  }

  findAll() {
    return this.userRepository.find();
  }

  findOne(id: string) {
    return this.userRepository.findOne({ userId: id });
  }

  findByEmail(email: string) {
    return this.userRepository.findOne({ email });
  }

  update(id: string, updateUserDto: UpdateUserDto) {
    return this.userRepository.updateOne(
      { userId: id },
      { $set: updateUserDto },
    );
  }

  remove(id: string) {
    return this.userRepository.delete({ userId: id });
  }
}
