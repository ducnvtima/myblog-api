import { Injectable } from '@nestjs/common';
import { CreateResumeDto } from './dto/create-resume.dto';
import { UpdateResumeDto } from './dto/update-resume.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Resume } from './entities/resume.entity';
import { MongoRepository } from 'typeorm';
import { v4 as randomUUID } from 'uuid';

@Injectable()
export class ResumeService {
  constructor(
    @InjectRepository(Resume)
    private readonly resumeRepository: MongoRepository<Resume>,
  ) {}

  create(createResumeDto: CreateResumeDto) {
    createResumeDto.resumeId = randomUUID();
    return this.resumeRepository.save(createResumeDto);
  }

  findAll() {
    return this.resumeRepository.find();
  }

  findOne(id: string) {
    return this.resumeRepository.findOne({ resumeId: id });
  }

  update(id: string, updateResumeDto: UpdateResumeDto) {
    return this.resumeRepository
      .updateOne({ resumeId: id }, { $set: updateResumeDto })
      .then((result) => ({ result: result.result.ok }));
  }

  remove(id: string) {
    return this.resumeRepository
      .deleteOne({ resumeId: id })
      .then((result) => ({ result: result.result.ok }));
  }
}
