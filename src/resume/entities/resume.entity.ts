import { Column, Entity, ObjectIdColumn } from 'typeorm';
import { ObjectID } from 'typeorm';
import {
  IsArray,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@Entity('Resume')
export class Resume {
  @ObjectIdColumn()
  id: ObjectID;

  @IsString()
  @Column()
  resumeId: string;

  @IsNumber()
  @Column()
  @ApiProperty({
    description: 'From Date',
    default: 1623315068,
  })
  fromDate: number;

  @IsNumber()
  @Column()
  @ApiProperty({
    description: 'To Date',
    default: 1623315068,
  })
  toDate: number;

  @IsString()
  @Column()
  @ApiProperty({
    description: 'Company Name',
    default: 'GOOGLE INC.',
  })
  companyName: string;

  @IsString()
  @Column()
  @ApiProperty({
    description: 'Position',
    default: 'Developer',
  })
  position: string;

  @IsString()
  @Column()
  @ApiProperty({
    description: 'Description job',
    default:
      'I worked as a Web Developer at Google for 3 years. I create usable web interfaces, front end coding stuff and almost anything. But i love what i do.',
  })
  description: string;

  constructor(resume?: Partial<Resume>) {
    Object.assign(this, resume);
  }
}
