import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { AlbumsModule } from './albums/albums.module';
import { ArticlesModule } from './articles/articles.module';
import { ProjectsModule } from './projects/projects.module';
import { ResumeModule } from './resume/resume.module';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url:
        // 'mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false',
      // :
          'mongodb+srv://ducnv:Anhduc1234@cluster0.df3oj.mongodb.net/myblog?retryWrites=true&w=majority',
      database: 'myblog',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      useUnifiedTopology: true,
      useNewUrlParser: true,
    }),
    UsersModule,
    AlbumsModule,
    ArticlesModule,
    ProjectsModule,
    ResumeModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AuthService],
})
export class AppModule {}
