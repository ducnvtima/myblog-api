import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class LoginDto {
  @ApiProperty({
    description: 'Địa chỉ email',
    default: 'demo@gmail.com',
  })
  username: string;

  @ApiProperty({
    description: 'Mật khẩu định đạng MD5',
    default: 'e10adc3949ba59abbe56e057f20f883e',
  })
  password: string;
}
