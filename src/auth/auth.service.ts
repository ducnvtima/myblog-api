import { BadRequestException, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.usersService.findByEmail(username);
    if (user) {
      if (user.password === password) {
        const { password, ...result } = user;
        return result;
      } else {
        throw new BadRequestException(['Mật khẩu không đúng!']);
      }
    }
    throw new BadRequestException(['Địa chỉ email không đúng!']);
  }

  async login(user: any) {
    const payload = { username: user.email, userId: user.userId };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
