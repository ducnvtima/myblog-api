import { ApiProperty, OmitType, PickType } from '@nestjs/swagger';
import { Article } from '../entities/article.entity';

export class CreateArticleDto extends OmitType(Article, [
  'id',
  'time',
]) {
}
