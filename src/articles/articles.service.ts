import { Injectable } from '@nestjs/common';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Article } from './entities/article.entity';
import { MongoRepository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectRepository(Article)
    private readonly articleRepository: MongoRepository<Article>,
  ) {}

  create(createArticleDto: CreateArticleDto) {
    createArticleDto.articleId = uuidv4();
    return this.articleRepository.save(createArticleDto);
  }

  findAll() {
    return this.articleRepository.find({ take: 20 });
  }

  findOne(id: string) {
    return this.articleRepository.findOne({
      articleId: id,
    });
  }

  update(id: string, updateArticleDto: UpdateArticleDto) {
    return this.articleRepository.updateOne(
      { articleId: id },
      { $set: updateArticleDto },
    );
  }

  remove(id: string) {
    return this.articleRepository.delete({ articleId: id });
  }
}
