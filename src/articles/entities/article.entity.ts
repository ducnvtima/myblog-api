import { Column, Entity, ObjectIdColumn } from 'typeorm';
import { ObjectId } from 'mongodb';
import { ApiProperty } from '@nestjs/swagger';

@Entity('Article')
export class Article {
  @ObjectIdColumn()
  id: ObjectId;

  @Column()
  articleId: string;

  @ApiProperty({})
  @Column()
  name: string;

  @ApiProperty({})
  @Column()
  time: number;

  @ApiProperty({})
  @Column()
  type: ArticleType;

  @ApiProperty({})
  @Column()
  title: string;

  @ApiProperty({})
  @Column()
  description: string;

  @ApiProperty({})
  @Column()
  category: string;

  @ApiProperty({})
  @Column()
  tag: [string];

  @ApiProperty({})
  @Column()
  images: [string];

  @ApiProperty({})
  @Column()
  cover: string;

  @ApiProperty({})
  @Column()
  urlSound: string;

  @ApiProperty({})
  @Column()
  urlYoutube: string;

  @ApiProperty({})
  @Column()
  content: string;

  @ApiProperty({})
  @Column()
  author: string;

  constructor(article?: Partial<Article>) {
    Object.assign(this, article);
  }
}
