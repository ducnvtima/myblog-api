import { Injectable } from '@nestjs/common';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { Project } from './entities/project.entity';
import { v4 as randomUUID } from 'uuid';

@Injectable()
export class ProjectsService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: MongoRepository<Project>,
  ) {}

  create(createProjectDto: CreateProjectDto) {
    createProjectDto.projectId = randomUUID();
    return this.projectRepository.save(createProjectDto);
  }

  findAll() {
    return this.projectRepository.find();
  }

  findOne(id: string) {
    return this.projectRepository.findOne({ projectId: id });
  }

  update(id: string, updateProjectDto: UpdateProjectDto) {
    return this.projectRepository
      .updateOne({ projectId: id }, { $set: updateProjectDto })
      .then((result) => ({ result: result.result.ok }));
  }

  remove(id: string) {
    return this.projectRepository
      .deleteOne({ projectId: id })
      .then((result) => ({ result: result.result.ok }));
  }
}
