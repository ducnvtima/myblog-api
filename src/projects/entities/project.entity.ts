import { Column, Entity, ObjectIdColumn } from 'typeorm';
import { ObjectID } from 'typeorm';
import {
  IsArray,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

@Entity('Project')
export class Project {
  @ObjectIdColumn()
  id: ObjectID;

  @IsString()
  @Column()
  projectId: string;

  @IsString()
  @IsNotEmpty()
  @Column()
  @ApiProperty({
    description: 'Tên project',
    default: `Nguyen Duc's blog`,
  })
  name: string;

  @Column()
  @ApiProperty({
    description: 'Mô tả',
    default: 'Descriptions',
  })
  description: string;

  @IsNotEmpty()
  @IsString()
  @Column()
  @ApiProperty({
    description: 'Mật khẩu định đạng MD5',
    default: 'e10adc3949ba59abbe56e057f20f883e',
  })
  password: string;

  @IsString()
  @Column()
  @ApiProperty({
    description: 'Ảnh cover',
    default: 'https://i.imgur.com/XwubuzF.jpg',
  })
  cover: string;

  @IsString()
  @Column()
  @ApiProperty({
    description: 'Trang chủ',
    default: 'http://www.ducnv.name.vn',
  })
  website: string;

  @IsString()
  @Column()
  @ApiProperty({
    description: 'Link iOS',
    default: 'https://apps.apple.com/vn/app/i-link/id1497299208',
  })
  appleLink: string;

  @IsString()
  @Column()
  @ApiProperty({
    description: 'Link Android',
    default: 'https://play.google.com/store/apps/details?id=vn.shopf1.android',
  })
  googleLink: string;

  @IsArray()
  @Column()
  @ApiProperty({
    description: 'Ảnh project',
    default: [
      'https://play-lh.googleusercontent.com/z8hCz7WCfA5k9vBnrDRqcyMpAnEoDDdeG0qU-enKR89IVhidt2PjEWcvkYB61ZM6Vo9R=w1440-h620-rw',
    ],
  })
  images: [string];

  constructor(project?: Partial<Project>) {
    Object.assign(this, project);
  }
}
