import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { IsArray, IsString } from 'class-validator';
import * as mongodb from 'mongodb';

@Entity('Album')
export class Album {
  @ObjectIdColumn()
  id: mongodb.ObjectId;

  @Column()
  albumId: string;

  @ApiProperty({
    default: 'Album 1',
  })
  @IsString()
  @Column()
  name: string;

  @ApiProperty({
    default: 'title',
  })
  @IsString()
  @Column()
  title: string;

  @Column()
  time: number;

  @ApiProperty({
    default: 'https://i.imgur.com/XwubuzF.jpg',
  })
  @IsString()
  @Column()
  thumbnail: string;

  @ApiProperty({
    default: ['travel'],
  })
  @IsArray()
  @Column()
  category: [string];

  constructor(album?: Partial<Album>) {
    Object.assign(this, album);
  }
}
