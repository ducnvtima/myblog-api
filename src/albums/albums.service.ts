import { Injectable } from '@nestjs/common';
import { CreateAlbumDto } from './dto/create-album.dto';
import { UpdateAlbumDto } from './dto/update-album.dto';
import { Album } from './entities/album.entity';
import { MongoRepository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { v4 as randomUUID } from 'uuid';

@Injectable()
export class AlbumsService {
  constructor(
    @InjectRepository(Album)
    private readonly albumRepository: MongoRepository<Album>,
  ) {}

  create(createAlbumDto: CreateAlbumDto) {
    createAlbumDto.albumId = randomUUID();
    createAlbumDto.time = new Date().getTime();
    return this.albumRepository.save(createAlbumDto);
  }

  findAll() {
    return this.albumRepository.find();
  }

  findOne(id: string) {
    return this.albumRepository.findOne({ albumId: id });
  }

  update(id: string, updateAlbumDto: UpdateAlbumDto) {
    return this.albumRepository.updateOne(
      { albumId: id },
      { $set: updateAlbumDto },
    );
  }

  remove(id: string) {
    return this.albumRepository.deleteOne({ albumId: id });
  }
}
