import { OmitType } from '@nestjs/swagger';
import { Album } from '../entities/album.entity';

export class CreateAlbumDto extends OmitType(Album, ['id', 'albumId', 'time']) {
  albumId: string;
  time: number;
}
