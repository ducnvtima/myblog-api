enum ArticleType {
  READ = 1,
  IMAGE = 2,
  SOUNDCLOUD = 3,
  YOUTUBE = 4,
  QUOTE = 5,
}
